all:
	dune build

clean:
	dune clean

fmt:
	dune build @fmt --auto-promote

test:
	dune runtest

doc:
	dune build @doc

.PHONY:	all clean fmt test
