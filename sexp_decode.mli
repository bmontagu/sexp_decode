(** A library of monadic combinators for decoding S-expressions (as
    defined in the [Csexp] library) into structured data.
    @see <https://github.com/ocaml-dune/csexp>

    Copyright © Inria 2022

    @author Benoît Montagu <benoit.montagu@inria.fr>
*)

include Sexp_decode_intf.INTF
