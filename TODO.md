- Add an alternative implementation with complete backtracking. The
  ``run`` function would then return a sequence of possible results.

- Add an alternative implementation that records information to ease
  debugging the design of decoders.
